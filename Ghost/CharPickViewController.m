//
//  InitViewController.m
//  Ghost
//
//  Created by David Gao on 5/21/14.
//  Copyright (c) 2014 davidgao. All rights reserved.
//

#import "CharPickViewController.h"

@interface CharPickViewController ()

@end

@implementation CharPickViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.numGhosts.delegate = self;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backButton:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)nextButton:(id)sender
{
    WordViewController *wordView = [self.storyboard instantiateViewControllerWithIdentifier:@"WordViewController"];
    if([self.numHumans.text isEqualToString:@""]) self.numHumans.text = @"1";
    if([self.numCrazies.text isEqualToString:@""]) self.numCrazies.text = @"1";
    if([self.numGhosts.text isEqualToString:@""]) self.numGhosts.text = @"1";
    if ([self.numHumans.text integerValue]+[self.numCrazies.text integerValue] > [self.numGhosts.text integerValue]) {
        [wordView getHumans:self.numHumans.text crazies:self.numCrazies.text ghosts:self.numGhosts.text];
        [self.navigationController pushViewController:wordView animated:YES];
    }
    else {
        UIAlertView *ErrorAlert = [[UIAlertView alloc] initWithTitle:@""
                                                             message:@"Number of ghosts must be less than number of humans and crazies!" delegate:nil
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil, nil];
        [ErrorAlert show];
    }
}

- (void)dismissKeyboard
{
    [self.view endEditing:YES];
}
/*
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

-(void)keyboardWillShow:(NSNotification*)note {
    NSDictionary* info = [note userInfo];
    NSTimeInterval animationDuration = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration]; // to slide the view up
    
    CGRect rect = self.view.frame;
    // 1. move the origin of view up so that the text field will come above the keyboard
    rect.origin.y -= kbSize.height/2;
    // 2. increase the height of the view to cover up the area behind the keyboard
    rect.size.height += kbSize.height/2;
    
    self.view.frame = rect;
    [UIView commitAnimations];
}

-(void)keyboardWillHide:(NSNotification*)note {
    NSDictionary* info = [note userInfo];
    NSTimeInterval animationDuration = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration]; // to slide the view up
    
    CGRect rect = self.view.frame;
    rect.origin.y += kbSize.height/2;
    rect.size.height -= kbSize.height/2;
    
    self.view.frame = rect;
    [UIView commitAnimations];
}*/
@end
