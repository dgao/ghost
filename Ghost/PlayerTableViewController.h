//
//  PlayerTableViewController.h
//  Ghost
//
//  Created by David Gao on 6/14/14.
//  Copyright (c) 2014 davidgao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlayerTableViewController : UITableViewController <UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate>

- (void)passArray:(NSMutableArray *)name and:(NSMutableArray *)role passInt:(NSInteger)human and:(NSInteger)ghost;

@end
