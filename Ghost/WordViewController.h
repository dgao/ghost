//
//  WordViewController.h
//  Ghost
//
//  Created by David Gao on 6/6/14.
//  Copyright (c) 2014 davidgao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlayerTableViewController.h"

@interface WordViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *wordID;
@property (strong, nonatomic) IBOutlet UIButton *hideWord;
@property (strong, nonatomic) IBOutlet UIButton *nextButton;
@property (strong, nonatomic) IBOutlet UIButton *startButton;
@property (strong, nonatomic) IBOutlet UITextField *textField;
- (IBAction)backButton:(id)sender;
- (IBAction)nextButton:(id)sender;
- (IBAction)startButton:(id)sender;
- (void)getHumans:(NSString *)humanStr crazies:(NSString *)crazyStr ghosts:(NSString *)ghostStr;

@end
