//
//  WordViewController.m
//  Ghost
//
//  Created by David Gao on 6/6/14.
//  Copyright (c) 2014 davidgao. All rights reserved.
//

#import "WordViewController.h"

@interface WordViewController ()

@end

@implementation WordViewController
{
    NSMutableArray *playerNames;
    NSMutableArray *playerRoles;
    NSMutableArray *playerWords;
    NSInteger numHumans;
    NSInteger numCrazies;
    NSInteger numGhosts;
    NSInteger index;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    index = 0;
    self.wordID.hidden = YES;
    self.nextButton.hidden = YES;
    self.startButton.hidden = YES;
    UILongPressGestureRecognizer *longPress_gr = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(doAction:)];
    [longPress_gr setMinimumPressDuration:0.6]; // triggers the action after 1 second of press
    [self.hideWord addGestureRecognizer:longPress_gr];
    // Do any additional setup after loading the view.
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
}

- (void)doAction:(UILongPressGestureRecognizer *)recognizer
{
    [self.view endEditing:YES];
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        self.wordID.text = [playerWords objectAtIndex:index];
        self.hideWord.hidden = YES;
        self.wordID.hidden = NO;
        self.nextButton.hidden = NO;
        if (index >= playerRoles.count-1) {
            self.nextButton.hidden = YES;
            self.startButton.hidden = NO;
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dismissKeyboard
{
    [self.view endEditing:YES];
}

- (void)getHumans:(NSString *)humanStr crazies:(NSString *)crazyStr ghosts:(NSString *)ghostStr
{
    playerNames = [[NSMutableArray alloc] init];
    playerRoles = [[NSMutableArray alloc] init];
    playerWords = [[NSMutableArray alloc] init];
    numHumans = [humanStr integerValue];
    numCrazies = [crazyStr integerValue];
    numGhosts = [ghostStr integerValue];
    if(!numHumans) numHumans++;
    if(!numCrazies) numCrazies++;
    if(!numGhosts) numGhosts++;
    for (int i = 0; i < numHumans; i++) {
        [playerRoles addObject:@"Human"];
        [playerWords addObject:@"Door"];
    }
    for (int i = 0; i < numCrazies; i++) {
        [playerRoles addObject:@"Crazy"];
        [playerWords addObject:@"Wood"];
    }
    for (int i = 0; i < numGhosts; i++) {
        [playerRoles addObject:@"Ghost"];
        [playerWords addObject:@"Ghost"];
    }
    for (int i = 0; i < playerRoles.count; i++) {
        int r = arc4random_uniform((int)playerRoles.count);
        [playerRoles exchangeObjectAtIndex:i withObjectAtIndex:r];
        [playerWords exchangeObjectAtIndex:i withObjectAtIndex:r];
    }
}

- (IBAction)backButton:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)nextButton:(id)sender
{
    if ([self.textField.text length] < 2) {
        [self NameError];
    }
    else {
        [UIView transitionWithView:self.view
                          duration:0.5f
                           options:UIViewAnimationOptionTransitionCurlUp
                        animations:^ { self.hideWord.hidden = NO;
                            self.wordID.hidden = YES;
                            self.nextButton.hidden = YES;
                            [playerNames addObject:self.textField.text];
                            self.textField.text = @"";
                            index++; }
                        completion:nil];
    }
}

- (IBAction)startButton:(id)sender
{
    if ([self.textField.text length] < 2) {
        [self NameError];
    }
    else {
        [playerNames addObject:self.textField.text];
        PlayerTableViewController *nextView = [self.storyboard instantiateViewControllerWithIdentifier:@"PlayerTableViewController"];
        [nextView passArray:playerNames and:playerRoles passInt:numHumans and:numGhosts];
        [self.navigationController pushViewController:nextView animated:YES];
    }
}

- (void)NameError
{
    UIAlertView *ErrorAlert = [[UIAlertView alloc] initWithTitle:@""
                                                         message:@"Name must be at least 2 characters" delegate:nil
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil, nil];
    [ErrorAlert show];
}
@end
