//
//  PageContentController.m
//  Ghost
//
//  Created by David Gao on 5/22/14.
//  Copyright (c) 2014 davidgao. All rights reserved.
//

#import "PageContentController.h"

@interface PageContentController ()

@end

@implementation PageContentController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.iconView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@", self.imageFile]];
    self.character.text = self.name;
    self.description.text = self.desc;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end