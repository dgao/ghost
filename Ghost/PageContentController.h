//
//  PageContentController.h
//  Ghost
//
//  Created by David Gao on 5/22/14.
//  Copyright (c) 2014 davidgao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PageContentController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *character;
@property (weak, nonatomic) IBOutlet UIImageView *iconView;
@property (weak, nonatomic) IBOutlet UILabel *description;
@property NSUInteger pageIndex;
@property NSString *desc;
@property NSString *name;
@property NSString *imageFile;

@end
