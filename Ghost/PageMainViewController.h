//
//  PageMainViewController.h
//  Ghost
//
//  Created by David Gao on 6/6/14.
//  Copyright (c) 2014 davidgao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PageContentController.h"

@interface PageMainViewController : UIViewController <UIPageViewControllerDataSource>

- (IBAction)startWalkthrough:(id)sender;
@property (strong, nonatomic) UIPageViewController *pageViewController;
@property (strong, nonatomic) NSArray *pageTitles;
@property (strong, nonatomic) NSArray *pageImages;
@property (strong, nonatomic) NSArray *descriptions;
@property NSInteger pageCount;

@end
