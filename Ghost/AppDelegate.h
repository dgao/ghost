//
//  AppDelegate.h
//  Ghost
//
//  Created by David Gao on 5/21/14.
//  Copyright (c) 2014 davidgao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
