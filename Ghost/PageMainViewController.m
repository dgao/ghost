//
//  PageMainViewController.m
//  Ghost
//
//  Created by David Gao on 6/6/14.
//  Copyright (c) 2014 davidgao. All rights reserved.
//

#import "PageMainViewController.h"

@interface PageMainViewController ()

@end

@implementation PageMainViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    // Find out the path of recipes.plist
    NSString *path = [[NSBundle mainBundle] pathForResource:@"instructions" ofType:@"plist"];
    
    // Load the file content and read the data into arrays
    NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:path];
    _pageTitles = [dict objectForKey:@"characters"];
    _pageImages = [dict objectForKey:@"images"];
    _descriptions = [dict objectForKey:@"descriptions"];
    self.pageCount = [self.pageTitles count]+1;
    
    // Create page view controller
    self.pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageMainViewController"];
    self.pageViewController.dataSource = self;
    
    PageContentController *startingViewController = [self viewControllerAtIndex:0];
    NSArray *viewControllers = @[startingViewController];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    // Change the size of page view controller
    self.pageViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - 60);
    
    [self addChildViewController:_pageViewController];
    [self.view addSubview:_pageViewController.view];
    [self.pageViewController didMoveToParentViewController:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)startWalkthrough:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (PageContentController *)viewControllerAtIndex:(NSUInteger)index
{
    if (([self.pageTitles count] == 0) || (index >= self.pageCount)) {
        return nil;
    }
    
    // Create a new view controller and pass suitable data.
    PageContentController *pageContentController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageContentController"];
    if(index == 0) {
        //First page
        pageContentController = [self.storyboard instantiateViewControllerWithIdentifier:@"FirstInstruct"];
    }
    else {
        //Template page
        pageContentController.imageFile = self.pageImages[index-1];
        pageContentController.name = self.pageTitles[index-1];
        pageContentController.desc = self.descriptions[index-1];
        pageContentController.pageIndex = index;
    }
    return pageContentController;
}

#pragma mark - Page View Controller Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = ((PageContentController*) viewController).pageIndex;
    
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    index--;
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = ((PageContentController*) viewController).pageIndex;
    
    if (index == NSNotFound) {
        return nil;
    }
    
    index++;
    if (index == self.pageCount) {
        return nil;
    }
    return [self viewControllerAtIndex:index];
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
    return self.pageCount;
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
{
    return 0;
}

@end