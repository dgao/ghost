//
//  InitViewController.h
//  Ghost
//
//  Created by David Gao on 5/21/14.
//  Copyright (c) 2014 davidgao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WordViewController.h"
#import "PlayerTableViewController.h"

@interface CharPickViewController : UIViewController <UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UITextField *numHumans;
@property (strong, nonatomic) IBOutlet UITextField *numCrazies;
@property (strong, nonatomic) IBOutlet UITextField *numGhosts;
- (IBAction)backButton:(id)sender;
- (IBAction)nextButton:(id)sender;

@end
